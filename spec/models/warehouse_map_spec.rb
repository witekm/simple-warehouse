require_relative '../../app/models/warehouse_spot'
require_relative '../../app/models/warehouse_map'

describe WarehouseMap do
  let(:params) { {x: 10, y: 10} }

  subject { described_class.new(params) }

  context 'when initializing with proper data' do

    it 'returns proper WarehouseMap record' do

      expect(subject.instance_variable_get(:@x)).to eq params[:x]
      expect(subject.instance_variable_get(:@y)).to eq params[:y]
    end
  end

  describe '#store' do
    context 'when storing one element' do
      it 'adds element' do
        map = subject
        map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')

        expect(map.instance_variable_get(:@spots_sorted_set).size).to eq 18
        expect(map.instance_variable_get(:@crates_hash)["x2_y2"]).to eq(
                                                                         h: 3,
                                                                         w: 6,
                                                                         p: 'A'
                                                                     )
      end
    end

    context 'when storing two overlaping elements' do
      it 'stores only first and returns error WarehaouseMap::PositionTaken' do
        map = subject
        map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')

        expect{map.store(x: 4, y: 4, w: 2, h: 3, p: 'B')}.to raise_error(WarehouseMap::PositionTaken)
      end
    end

    context 'when trying to store a crate at a position which doesn\'t exist' do
      it 'returns error WarehouseMap::PositionDoesNotExist' do
        map = subject
        expect{map.store(x: 12, y: 14, w: 2, h: 3, p: 'B')}.to raise_error(WarehouseMap::PositionDoesNotExist)
      end
    end

    context 'when trying to store a crate which doesn\'t fit - too wide or too tall' do
      it 'returns error WarehouseMap::CrateDoesNotFit' do
        map = subject
        expect{map.store(x: 8, y: 8, w: 4, h: 4, p: 'B')}.to raise_error(WarehouseMap::CrateDoesNotFit)
      end
    end
  end

  describe '#locate' do
    context 'when map empty' do
      it 'returns error WarehouseMap::Empty' do
        expect{ subject.locate(p: 'A') }.to raise_error(WarehouseMap::Empty)
      end
    end

    context 'when map contains product' do
      it 'returns list of crates' do
        map = subject
        map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')
        list = subject.locate(p: 'A')

        expect(list).to be_kind_of(Array)
        expect(list.first).to eq "2 2 6 3 A"
      end
    end

    context 'when map does not contain product' do
      it 'returns error WarehouseMap::ProductNotFound' do
        map = subject
        map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')

        expect{ subject.locate(p: 'B') }.to raise_error(WarehouseMap::ProductNotFound)
      end
    end
  end

  describe '#remove' do
    context 'when crate not found' do
      it 'returns error WarehouseMap::CrateNotFound' do
        expect{ subject.remove(x: 2, y: 3) }.to raise_error(WarehouseMap::CrateNotFound)
      end
    end

    context 'when crate found' do
      it 'removes related spot sets' do
        map = subject
        map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')
        map.store(x: 9, y: 1, w: 2, h: 4, p: 'B')
        map.remove(x: 2, y: 2)

        expect(map.instance_variable_get(:@spots_sorted_set).size).to eq 8
      end

      it 'removes create details from crates hash' do
        map = subject
        map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')
        map.store(x: 9, y: 1, w: 2, h: 4, p: 'B')
        map.remove(x: 2, y: 2)

        expect(map.instance_variable_get(:@crates_hash).size).to eq 1
      end
    end
  end

  describe '#to_s' do
    it 'returns view of warehouse' do
      map = subject
      map.store(x: 2, y: 2, w: 6, h: 3, p: 'A')
      map.store(x: 9, y: 1, w: 2, h: 4, p: 'B')

      output = map.to_s.split
      expect(output[5]).to eq "**********"
      expect(output[6]).to eq "*AAAAAA*BB"
      expect(output[7]).to eq "*AAAAAA*BB"
      expect(output[8]).to eq "*AAAAAA*BB"
      expect(output[9]).to eq "********BB"
    end
  end
end