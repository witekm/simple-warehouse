require_relative '../../app/models/warehouse_spot'

describe WarehouseSpot do
  context 'when initializing with proper data' do
    let(:params) { {x: 1, y: 3, p: 'C'} }

    subject { described_class.new(params) }

    it 'returns proper WarehouseSpot record' do

      expect(subject.x).to eq params[:x]
      expect(subject.y).to eq params[:y]
      expect(subject.p).to eq params[:p]
    end
  end

  context 'when comparing' do
    it 'assesses records by y position descending and x position ascending' do
      record_x1_y1 = described_class.new(x: 1, y: 1, p: 'A')
      record_x2_y2 = described_class.new(x: 2, y: 2, p: 'C')

      expect(record_x1_y1 > record_x2_y2).to be true
      expect(record_x2_y2 < record_x1_y1).to be true
      expect(record_x1_y1 == record_x2_y2).to be false
    end
  end
end