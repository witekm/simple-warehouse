require 'ruby_helper'
require_relative '../../app/services/handle_initialization'

describe HandleInitialization do
  let(:proper_command) { "init 10 8" }
  let(:wrong_command)  { "init 2"}

  subject { described_class }

  context 'when initializing with proper data' do

    it 'returns WarehouseMap in data' do
      expect(subject.call(proper_command).data).to be_kind_of(WarehouseMap)
    end

    it 'returns success message' do
      expect(subject.call(proper_command).to_s).to match /Warehouse initialized successfully!/
    end
  end

  context 'when initializing with wrong data' do
    it 'returns error message' do
      expect(subject.call(wrong_command).to_s).to match /Warehouse has not been created!/
    end
  end

end
