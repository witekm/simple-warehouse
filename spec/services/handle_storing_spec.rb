require 'ruby_helper'
require_relative '../../app/services/handle_storing'

describe HandleStoring do
  let(:proper_command) { "store 2 2 3 4 A" }
  let(:warehouse_map) { WarehouseMap.new(x: 10, y: 10) }

  subject { described_class }

  context 'when initializing with proper command' do
    it 'creates HandleStoring' do
      handle_storing = subject.new(proper_command, warehouse_map)

      expect(handle_storing.instance_variable_get(:@x)).to eq 2
      expect(handle_storing.instance_variable_get(:@y)).to eq 2
      expect(handle_storing.instance_variable_get(:@w)).to eq 3
      expect(handle_storing.instance_variable_get(:@h)).to eq 4
    end
  end

  context 'when storing proper crate' do
    it 'returns changed map' do
      result = HandleStoring.call(proper_command, warehouse_map)

      expect(result).to be_kind_of(Result::Success)
      expect(result.data.instance_variable_get(:@spots_sorted_set).size).to eq 12
    end

    it 'has success message' do
      result = HandleStoring.call(proper_command, warehouse_map)
      expect(result.to_s).to match /Crate stored successfully!/
    end
  end

  context 'when storing proper crate but map not initialized' do
    it 'returns error message' do
      result = HandleStoring.call(proper_command, nil)
      expect(result.to_s).to match /Warehouse has not been initialized!/
    end
  end

  context 'when one of crate positions is taken' do
    it 'handles WarehouseMap::PositionTaken and returns error message' do
      wh_map = warehouse_map
      wh_map.store(x: 2, y: 3, w: 5, h: 5, p: 'A')

      result = HandleStoring.call("store 5 5 2 2 B", wh_map)
      expect(result.to_s).to match /Create was not added! Conflict with existing crates!/
    end
  end

  context 'when starting crate position does not exist' do
    it 'handles WarehouseMap::PositionDoesNotExist and returns error message' do
      wh_map = warehouse_map

      result = HandleStoring.call("store 12 5 2 2 B", wh_map)
      expect(result.to_s).to match /Create was not added! Starting point outside WH area!/
    end
  end

  context 'when crate does not fit' do
    context 'with size too big' do
      it 'handles WarehouseMap::CrateDoesNotFit and returns error message' do
        wh_map = warehouse_map

        result = HandleStoring.call("store 8 8 5 5 B", wh_map)
        expect(result.to_s).to match /Create was not added! It's too big!/
      end
    end
  end

end