require 'ruby_helper'
require_relative '../../app/services/handle_removal'

describe HandleRemoval do
  let(:warehouse_map) { WarehouseMap.new(x: 10, y: 10).store(x: 2, y: 2, w: 3, h: 4, p: 'A') }

  subject { described_class }

  context 'when crate not found' do
    it 'returns error message' do
      result = subject.call("remove 3 4", warehouse_map)

      expect(result).to be_kind_of(Result::Error)
      expect(result.to_s).to match /Given crate does not exist!/
    end
  end

  context 'when crate found' do
    it 'returns map without crate' do
      result = subject.call("remove 2 2", warehouse_map)

      expect(result).to be_kind_of(Result::Success)
      expect(result.data.instance_variable_get(:@spots_sorted_set)).to be_empty
    end

    it 'returns success message' do
      result = subject.call("remove 2 2", warehouse_map)

      expect(result).to be_kind_of(Result::Success)
      expect(result.to_s).to match /Crate removed successfully!/
    end
  end
end

