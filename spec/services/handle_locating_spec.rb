require 'ruby_helper'
require_relative '../../app/services/handle_locating'

describe HandleLocating do
  let(:proper_command) { "store 2 2 3 4 A" }
  let(:warehouse_map) { WarehouseMap.new(x: 10, y: 10) }

  subject { described_class }

  context 'when warehouse empty' do
    it 'returns error message' do
      result = subject.call("locate A", warehouse_map)

      expect(result).to be_kind_of(Result::Error)
      expect(result.to_s).to match /Warehouse is empty! Product not found!/
    end
  end

  context 'when product not found' do
    it 'returns error message' do
      wh_map = warehouse_map.store(x: 1, y: 1, w: 2, h: 2, p: 'A')
      result = subject.call("locate B", wh_map)

      expect(result).to be_kind_of(Result::Error)
      expect(result.to_s).to match /Product not found!/
    end
  end

  context 'when product found' do
    it 'returns list of products' do
      wh_map = warehouse_map.store(x: 1, y: 1, w: 2, h: 2, p: 'A')
      result = subject.call("locate A", wh_map)

      expect(result).to            be_kind_of(Result::Success)
      expect(result.data).to       be_kind_of(Array)
      expect(result.data.first).to eq '1 1 2 2 A'
    end

    it 'returns success message' do
      wh_map = warehouse_map.store(x: 1, y: 1, w: 2, h: 2, p: 'A')
      result = subject.call("locate A", wh_map)

      expect(result).to be_kind_of(Result::Success)
      expect(result.to_s).to match /Product located successfully!/
    end
  end
end
