class HandleLocating
  class << self
    def call(command, map)
      new(command, map).call()
    end
  end

  def initialize(command, map)
    @p   = command.split.last
    @map = map
  end

  def call
    Result::Success.new(@map.locate(p: @p), 'Product located successfully!')

  rescue WarehouseMap::Empty
    Result::Error.new('Warehouse is empty! Product not found!')
  rescue WarehouseMap::ProductNotFound
    Result::Error.new('Product not found!')
  end
end
