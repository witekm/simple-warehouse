class HandleRemoval
  class << self
    def call(command, map)
      new(command, map).call()
    end
  end

  def initialize(command, map)
    @x, @y = command.split.last(2).map(&:to_i)
    @map   = map
  end

  def call
    Result::Success.new(@map.remove(x: @x, y: @y), 'Crate removed successfully!')
  rescue WarehouseMap::CrateNotFound
    Result::Error.new('Given crate does not exist!')
  end
end

