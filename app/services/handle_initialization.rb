class HandleInitialization
  class << self
    def call(command)
      new(command).call
    end
  end

  attr_reader :x, :y

  def initialize(command)
    numbers = command.split.last(2)
    @x      = numbers.first.to_i
    @y      = numbers.last.to_i
  end

  def call
    return ::Result::Error.new('Warehouse has not been created!') if x == 0 || y == 0

    ::Result::Success.new(WarehouseMap.new(x: x, y: y), 'Warehouse initialized successfully!')
  end
end