module Result
  class Success
    attr_reader :data, :notice

    def initialize(data, notice = '')
      @data = data
      @notice = notice
    end

    def to_s
      notice
    end

    def success?
      true
    end
  end
end