module Result
  class Error
    attr_reader :error

    def initialize(error)
      @error = error
    end

    def to_s
      error
    end

    def success?
      false
    end
  end
end