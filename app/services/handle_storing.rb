class HandleStoring
  class << self
    def call(command, map)
      new(command, map).call()
    end
  end

  def initialize(command, map)
    data = command.split
    @x, @y, @w, @h = data[1..-1].first(4).map(&:to_i)
    @p             = data.last
    @map           = map
  end

  def call
    return ::Result::Error.new('Warehouse has not been initialized!') if @map.nil?

    @map.store(x: @x, y: @y, w: @w, h: @h, p: @p)

    return ::Result::Success.new(@map, 'Crate stored successfully!')
  rescue WarehouseMap::PositionTaken
    ::Result::Error.new('Create was not added! Conflict with existing crates!')
  rescue WarehouseMap::PositionDoesNotExist
    ::Result::Error.new('Create was not added! Starting point outside WH area!')
  rescue WarehouseMap::CrateDoesNotFit
    ::Result::Error.new('Create was not added! It\'s too big!')
  end
end