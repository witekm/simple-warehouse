require 'set'

class WarehouseMap

  PositionTaken        = Class.new(StandardError)
  PositionDoesNotExist = Class.new(StandardError)
  CrateDoesNotFit      = Class.new(StandardError)

  Empty                = Class.new(StandardError)
  ProductNotFound      = Class.new(StandardError)

  CrateNotFound        = Class.new(StandardError)

  def initialize(x:, y:)
    @x = x
    @y = y
    @spots_sorted_set = SortedSet.new
    @crates_hash = Hash.new()
  end

  def locate(p:)
    raise Empty if @crates_hash.empty?

    product_list = @crates_hash.select { |_, v| v.fetch(:p) == p }

    raise ProductNotFound if product_list.empty?
    product_list.map{ |crate_key, crate_value| "#{crate_key.delete('xy').split("_").join(" ")} #{crate_value.values.join(" ")}" }
  end

  def remove(x:, y:)
    crate_to_be_removed = @crates_hash["x#{x}_y#{y}"]

    raise CrateNotFound if crate_to_be_removed.nil?

    x_max = x + crate_to_be_removed[:w] - 1
    y_max = y + crate_to_be_removed[:h] - 1

    @spots_sorted_set.delete_if { |spot| spot.x.between?(x, x_max) && spot.y.between?(y, y_max) }
    @crates_hash.delete("x#{x}_y#{y}")
    self
  end

  def store(x:, y:, w:, h:, p:)
    x_end = x + w - 1
    y_end = y + h - 1

    raise PositionDoesNotExist unless x.between?(1, @x) and y.between?(1, @y)
    raise CrateDoesNotFit      unless (x_end).between?(1, @x) and (y_end).between?(1, @y)

    current_sorted_set = @spots_sorted_set

    x.upto(x_end) do |x_index|
      y.upto(y_end) do |y_index|
        spot = WarehouseSpot.new(x: x_index, y: y_index, p: p)
        raise PositionTaken if current_sorted_set.add?(spot).nil?
      end
    end

    @spots_sorted_set           = current_sorted_set
    @crates_hash["x#{x}_y#{y}"] = {w: w, h: h, p: p}
    self
  end

  def to_s
    output                   = ""
    copy_of_spots_sorted_set = @spots_sorted_set.dup
    next_spot                = copy_of_spots_sorted_set.first

    @y.downto(1) do |y_index|
      1.upto(@x) do |x_index|
        if position_of_next_spot?(next_spot, x_index, y_index)
          output << next_spot.p
          copy_of_spots_sorted_set.delete(next_spot)
          next_spot = copy_of_spots_sorted_set.first
        else
          output << '*'
        end
      end
      output << "\n"
    end
    output
  end

  private

  def position_of_next_spot?(next_spot, x_index, y_index)
    !next_spot.nil? && next_spot.x == x_index && next_spot.y == y_index
  end

end