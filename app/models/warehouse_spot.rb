class WarehouseSpot
  include Comparable

  attr_reader :x, :y, :p

  def initialize(x:, y:, p:)
    @x = x
    @y = y
    @p = p
  end

  def <=>(other)
    [other.y, x] <=> [y, other.x] #asc order for x and desc order for y to allow easy print
  end

  def eql?(other)
    x == other.x && y == other.y
  end

  def hash
    x.hash ^ y.hash
  end

end