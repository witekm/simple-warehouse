Dir["#{__dir__}/models/*.rb"].each {|file| require file }
Dir["#{__dir__}/services/*.rb"].each {|file| require file }
Dir["#{__dir__}/services/result/*.rb"].each {|file| require file }

class SimpleWarehouse

  def run
    @live = true
    puts 'Type `help` for instructions on usage'
    while @live
      print '> '
      command = gets.chomp
      case
        when command == 'help'             then show_help_message
        when command.start_with?('init')   then handle_initialization(command)
        when command.start_with?('store')  then handle_storing(command)
        when command.start_with?('locate') then handle_locating(command)
        when command.start_with?('remove') then handle_removal(command)
        when command == 'view'             then handle_view
        when command == 'exit'             then exit
        else show_unrecognized_message
      end
    end
  end

  private

  def handle_initialization(command)
    result = ::HandleInitialization.call(command)

    @warehouse_map = result.data if result.success?

    puts result
  end

  def handle_storing(command)
    puts ::HandleStoring.call(command, @warehouse_map)
  end

  def handle_locating(command)
    result = ::HandleLocating.call(command, @warehouse_map)

    puts result

    result.data.each { |location| puts location } if result.success?
  end

  def handle_removal(command)
    puts ::HandleRemoval.call(command, @warehouse_map)
  end

  def handle_view
    puts @warehouse_map
  end

  def show_help_message
    puts <<~HELP
      help             Shows this help message
      init W H         (Re)Initialises the application as an empty W x H warehouse.
      store X Y W H P  Stores a crate of product code P and of size W x H at position (X,Y).
      locate P         Show a list of all locations occupied by product code P.
      remove X Y       Remove the entire crate occupying the location (X,Y).
      view             Output a visual representation of the current state of the grid.
      exit             Exits the application.
    HELP
  end

  def show_unrecognized_message
    puts 'Command not found. Type `help` for instructions on usage'
  end

  def exit
    puts 'Thank you for using simple_warehouse!'
    @live = false
  end

end
