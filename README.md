The algorithmic task with plain ruby to prepare solution for warehouse management. Basically shows approach to services, models, errors and rspec.
====

# Simple Warehouse
This is a simple command-line warehouse management tool. The user can record the storage and removal of crates of variable sizes on a grid of dynamic 'shelves'.

It accepts the following 7 commands:

| Command | Description |
| --- | --- |
| `help` | Shows the help message. |
| `init W H` | (Re)Initialises the application as an empty W x H warehouse. |
| `store X Y W H P` | Stores a crate of product code P and of size W x H at position (X,Y).<br>The crate will occupy W x H locations on the grid.|
| `locate P` | Show a list of all locations occupied by product code P. |
| `remove X Y` | Remove the entire crate occupying the location (X,Y). |
| `view` | Output a visual representation of the current state of the grid.<br>Position (1,1) should be at the bottom left position on the grid and (1, H) should be the top left. |
| `exit` | Exits the application. |

- Arguments W, H, X and Y will always be integers, and P will always be a single character.
- You should not worry about validating the format of the input.
- A crate of dimensions 2 x 3 will occupy 6 locations in the grid.

The user should be shown a graceful error message when:
- Trying to store a crate at a position which doesn't exist.
- Trying to store a crate which doesn't fit.
- Trying to remove a crate which doesn't exist.

![](./example.svg)

## Task
Adapt the provided skeleton application with the functionality described above.  Feel free to improve the existing code as you see fit.

We recommend writing unit tests for your code in either RSpec or Minitest to ensure the correct functionality is
achieved.  However, to save time, we suggest you do not write full integration tests that simulate `stdin` and capture `stdout`.


Overwrite this `README`, outlining the reasoning behind your design decisions and any ways in which you think your code could be improved.  If you need to refer back to these instructions, [they are duplicated here](./INSTRUCTIONS.md).

Please return an archive (`.zip` or `tar.gz`) of your local repository.

### Alternatives to Ruby
If you feel your skills are better demonstrated in a different language, please feel free to submit your solution in the language of your choice.  Be sure to include full instructions on how to build and run your code.

### Solution / Reasoning behind technical decisions
1. Data structure

    Main decision here was about storage map representation. As it should address two aspects of main problem domain. First, how expensive (complexity-wise) will be inserting new crates/ modifying existing ones.
    Second, how easy and costly will it be to transfer that structure to perform various read operations on it (view, locate).
    
    a. HashMaps
    
    One of the simplest solution seemed to be map representation as X hashes of Y (if necessary for a row) subhashes keeping current product for taken spot. i.e. { x1: { y1: { p: 'A' } } }
    Nevertheless, going that route will determine, we will need to build custom operations which can be performed effectively on hash of hash keeping product. Also, we should take into consideration that,
    it requires also X hashes to be initialized from the scratch, what takes unnecessary memory at that time.
    
    b. SortedSet with keeping 4 corners of crate
    
    The most optimal way (complexity-wise) seems to be keeping points X,Y / X+W, Y / X, Y+H / X+W, Y + H in sorted set with p value and hash value for crate (ie. x2y2w3h4A ) inside. When new crate is given by the user, we should take starting point X, Y and see if
    it is within rectangular space of any previously added crates. To do that, we would select 2 closest points on the left(one closer to Y:Y_max line, second closer to Y:1 line), the same for 2 points on right. Having those 4 points and
    confirming they are part of the same crate will give us certainty, we can't put new crate here. Otherwise, we should check 3 outstanding corners of new crate. This solution requires, custom implementation of SortedSet or utilizing 4 different SortedSets with different
    comparison rules, so that I didn't pursue this way, even though it seems to be win-win solution taking into account memory taken vs. speed of insertion
    
    c. SortedSet with custom WarehouseSpot - IMPLEMENTED
    
    I have decided to go with solution where I keep the map of our storage in SortedSet where every existing spot in defined and described by WarehouseSpot instance which has product name and defined methods so that we can use it as element of Set.
    Apart form that, we have @crates_hash which keeps beginnings of all our crates and associated values like W, H and P. This way on one hand we can easily check in sorted set if certain spot is already taken, just be trying to include new element. On the other hand,
    we can use @crates_hash for operations like remove and locate. In this solution we are giving away fast input time for new crates in exchange of easy implementation and to some extend efficiently utilized memory, as we only keep data about taken spots.
    
    d. SortedSet with custom WarehouseSpot with randomization on insert
    
    Similar to implemented solution, but to optimized average time of inserting new crates. We could implement randomized mechanism to check if certain spot of new crate isn't taken. So far, we are going in linear direction from lowest x and lowest y.
    For pessimistic data, where spots close to top-right corner are often taken, this solution can protect us from pessimistic insertion time. Of course, to some extend as it will simply average time to insert new crate.

2. Code organization

    a. models
    
    I defined two main models. First WarehouseSpot determines how single spot on our map should behave in relation to whole map of spots.
    WarehouseMap is main class, keeping within map of spots (WarehouseSpots) and hash of crates. Also, it defines operations on the map from model perspective.
    
    b. services
    
    To handle user prompts, and to separate concerns of responsibility our services are determining what input actually was given to system, calls necessary model actions and if necessary turns custom exceptions to human readable error messages.
    
    c. error handling
    
    To divide responsibilities of classes appropriately, I assumed that model actions will raise exceptions which will be handled somewhere above model's aciton ivocation - ideally in service.
    
    d. test cases
    
    As suggested, tests covers model actions and services responsibilities with proper messaging to user.
    
4. Possible improvements

    - errors could be moved to separate folder under app/errors
    - also for better readability warehouse_map methods could be split to queries and commands and treated as such
    what would give us nice separation of code for every action
    - utilizing HashMap could give us better time on insert but I found it too simple to be good so that I came out with other options
